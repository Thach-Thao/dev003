/**
 * ローカルサーバーの立ち上げ
 */

const gulp = require("gulp");
const config = require("../config");
const setting = config.setting;
const $ = require("gulp-load-plugins")(config.loadPlugins);

const net = require("net");
const co = require("co");

gulp.task("serve", () => {
	co(function*() {
		let port = yield (() => {
			return co(function*() {
				const server = net.createServer();
				let port = null;

				server.on("listening", () => {
					port = server.address().port;
					server.close();
				});

				return new Promise((resolve, reject) => {
					server.on("close", () => resolve(port));
					server.on("error", err => reject(err));
					server.listen(0, "127.0.0.1");
				});
			});
		})();

		console.log("Use port " + port);

		$.php.server(
			{
				port: port,
				base: setting.server.base
			},
			() => {
				$.browserSync({
					url: "localhost",
					port: 4000,
					proxy: "localhost:" + port
				});
			}
		);
	}).catch(err => console.error(err));

	// gulp.watch（ファイル監視）
	gulp.watch(setting.html.dest + "**/*.php", ["php2html", "phpSync", "phpValidate"]);
	gulp.watch(setting.css.src + "**/*.scss", ["scss", "prettier_scss"]);
	gulp.watch(setting.js.src + "**/*.js", ["script", "prettier_js"]);

	//gulp関係のjsも整形してしまう
	gulp.watch("gulp/**/*.js", ["prettier_js"]);
});

gulp.task("disconnect", () => {
	$.php.closeServer();
});
