const gulp = require("gulp");
const config = require("../config");
const setting = config.setting;
const $ = require("gulp-load-plugins")(config.loadPlugins);

gulp.task("prettier_scss", () => {
	//SCSS用
	if (setting.css.prettier) {
		gulp.src([setting.css.src + "**/*.scss"])
			.pipe(
				$.prettierPlugin(
					{
						printWidth: 120,
						tabWidth: 4,
						useTabs: true
					},
					{
						filter: true
					}
				)
			)
			.pipe(gulp.dest(file => file.base));
	}
});

//JS用
gulp.task("prettier_js", () => {
	if (setting.js.prettier) {
		gulp.src([setting.js.src + "**/*.js", "gulp/**/*.js"])
			.pipe(
				$.prettierPlugin(
					{
						printWidth: 120,
						tabWidth: 4,
						useTabs: true,
						bracketSpacing: true
					},
					{
						filter: true
					}
				)
			)
			.pipe(gulp.dest(file => file.base));
	}
});
